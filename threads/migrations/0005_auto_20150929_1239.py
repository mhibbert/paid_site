# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('threads', '0004_auto_20150929_1235'),
    ]

    operations = [
        migrations.AddField(
            model_name='poll',
            name='thread',
            field=models.OneToOneField(null=True, to='threads.Thread'),
        ),
        migrations.AlterField(
            model_name='posts',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 29, 12, 39, 43, 578069, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='thread',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 29, 12, 39, 43, 577077, tzinfo=utc)),
        ),
    ]

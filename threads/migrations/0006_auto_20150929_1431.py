# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('threads', '0005_auto_20150929_1239'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='PollSubjects',
            new_name='PollSubject',
        ),
        migrations.AlterField(
            model_name='posts',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 29, 14, 31, 13, 1887, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='thread',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 29, 14, 31, 13, 961, tzinfo=utc)),
        ),
    ]

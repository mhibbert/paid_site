# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('threads', '0007_auto_20150930_0920'),
    ]

    operations = [
        migrations.AddField(
            model_name='vote',
            name='ip_address',
            field=models.CharField(default=b'0.0.0.0', max_length=15),
        ),
        migrations.AlterField(
            model_name='posts',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 30, 9, 33, 25, 187738, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='thread',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 30, 9, 33, 25, 186835, tzinfo=utc)),
        ),
    ]

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
import os
from settings import BASE_DIR, INSTALLED_APPS

DEBUG = True
TEMPLATE_DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

INSTALLED_APPS += ('debug_toolbar',)

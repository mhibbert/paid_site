from django.shortcuts import render
from threads.models import Subject

def home(request):
    return render(request, 'subjects.html', { 'subjects': Subject.objects.all() })